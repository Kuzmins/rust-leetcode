use std::collections::{HashMap, VecDeque};

// https://leetcode.com/problems/longest-repeating-character-replacement/
pub fn character_replacement(s: String, k: i32) -> i32 {
    let mut result = 0;
    let mut freq_map: HashMap<char, usize> = HashMap::new();
    let (mut l, mut r, mut max_f) = (0, 0, 0);
    while r < s.len() {
        let new_char = s.as_bytes()[r] as char;
        r += 1;
        let count = freq_map.entry(new_char).or_insert(0);
        *count += 1;
        max_f = max_f.max(*count);
        let new_len = r - l;
        if new_len - max_f <= k as usize {
            result = result.max(new_len)
        } else {
            let char_to_delete = s.as_bytes()[l] as char;
            let count = freq_map.entry(char_to_delete).or_insert(0);
            *count -= 1;
            l += 1;
        }
    }

    result as i32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case1() {
        // Given
        let s = String::from("ABAB");

        // When
        let result = character_replacement(s, 2);

        // Then
        assert_eq!(result, 4);
    }

    #[test]
    fn case2() {
        // Given
        let s = String::from("AABABBA");

        // When
        let result = character_replacement(s, 1);

        // Then
        assert_eq!(result, 4);
    }
}
