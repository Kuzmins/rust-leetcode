use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    cost: usize,
    position: (usize, usize),
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        // Notice that the we flip the ordering on costs.
        // In case of a tie we compare positions - this step is necessary
        // to make implementations of `PartialEq` and `Ord` consistent.
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}
fn calculate_short_way2(
    start: (usize, usize),
    maze: &Vec<Vec<char>>,
    goal: (usize, usize),
    skip_c: bool,
) -> Vec<(usize, usize)> {
   let first =  calculate_short_way(start, maze, goal, skip_c, true);
   let second =  calculate_short_way(start, maze, goal, skip_c, false);
   if first.len() < second.len() {
       return first;
   } else {
    return second;
   }
}

fn calculate_short_way(
    start: (usize, usize),
    maze: &Vec<Vec<char>>,
    goal: (usize, usize),
    skip_c: bool,
    min_priority: bool,
) -> Vec<(usize, usize)> {
    eprintln!("finding short path from {:?} to {:?}", start, goal);
    let mut frontier: Vec<(usize, usize)> = Vec::new();
    let mut came_from: HashMap<(usize, usize), (usize, usize)> = HashMap::new();
    let mut cost_so_far: HashMap<(usize, usize), usize> = HashMap::new();
    let mut heap: BinaryHeap<State> = BinaryHeap::new();
    frontier.push(start);
    cost_so_far.insert(start, 0);
    came_from.insert(start, start);

    heap.push(State {
        cost: 0,
        position: start,
    });
    while heap.len() > 0 {
        let current_point = heap.pop().unwrap();
        if (current_point.position == goal) {
            break;
        }
        for w in get_possible_ways(current_point.position, maze).into_iter() {
            if (w.0 == 'C' && skip_c) {
                eprintln!("Skipping_c for map scan! {:?}", w.1);
                continue;
            }
            if !came_from.contains_key(&w.1) {
                let priority = if min_priority {heuristic(current_point.position, w.1)} else {heuristic(goal, w.1)};
                heap.push(State {
                    cost: priority,
                    position: w.1,
                });
                came_from.insert(w.1, current_point.position);
            }
        }
    }

    let mut current = goal;
    let mut path: Vec<(usize, usize)> = Vec::new();
    while current != start {
        path.push(current);
        current = came_from.get(&current).unwrap().clone();
    }
    eprintln!(
        "short_path from {:?} to {:?} is : {:?}",
        start,
        goal,
        path.len()
    );
    path
}

fn get_possible_ways(
    curr_pos: (usize, usize),
    maze: &Vec<Vec<char>>,
) -> Vec<(char, (usize, usize))> {
    let mut result: Vec<(char, (usize, usize))> = Vec::new();
    let up = maze
        .get(curr_pos.0 - 1)
        .and_then(|r| r.get(curr_pos.1))
        .filter(|c| **c == '.' || **c == 'C' || **c == 'T')
        .map(|c| (*c, (curr_pos.0 - 1, curr_pos.1)));
    let down = maze
        .get(curr_pos.0 + 1)
        .and_then(|r| r.get(curr_pos.1))
        .filter(|c| **c == '.' || **c == 'C' || **c == 'T')
        .map(|c| (*c, (curr_pos.0 + 1, curr_pos.1)));
    let left = maze
        .get(curr_pos.0)
        .and_then(|r| r.get(curr_pos.1 - 1))
        .filter(|c| **c == '.' || **c == 'C' || **c == 'T')
        .map(|c| (*c, (curr_pos.0, curr_pos.1 - 1)));
    let right = maze
        .get(curr_pos.0)
        .and_then(|r| r.get(curr_pos.1 + 1))
        .filter(|c| **c == '.' || **c == 'C' || **c == 'T')
        .map(|c| (*c, (curr_pos.0, curr_pos.1 + 1)));

    if let Some(u) = up {
        result.push(u);
    }

    if let Some(d) = down {
        result.push(d);
    }

    if let Some(r) = right {
        result.push(r);
    }

    if let Some(l) = left {
        result.push(l);
    }

    result
}

fn heuristic(from: (usize, usize), to: (usize, usize)) -> usize {
    let res = (from.0 as i32 - to.0 as i32).abs() + (from.1 as i32 - to.1 as i32).abs();
    println!("heuristic res from {:?} to {:?} is {}", from, to, res);
    res as usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case1() {
        // Given
        let mut maze_vec: Vec<Vec<char>> = Vec::new();

        let maze = r#"
##############################
#T...........................#
##...........................#
#............................#
#............................#
#............................#
#............................#
#............................#
#............................#
#............................#
#............................#
#............................#
#............................#
#...........................C#
##############################
"#
        .trim();
        // Calculating from C:(13, 10) to T:(1, 20) alarm: 42
        for row in maze.lines() {
            maze_vec.push(row.trim().chars().collect());
        }

        let way = calculate_short_way2((13, 28), &maze_vec, (1, 1), false);
        println!("Way: {:?}", way);
        println!("Way len: {:?}", way.len());
    }
}
